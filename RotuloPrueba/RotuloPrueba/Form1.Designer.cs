﻿namespace RotuloPrueba
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_RutaImg = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.writerPictureBox = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_destino = new System.Windows.Forms.TextBox();
            this.btn_Rotulo = new System.Windows.Forms.Button();
            this.txt_Eps = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_Nit = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_Factura = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_RadicacionNeps = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_FechaRad = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Txt_Valor = new System.Windows.Forms.TextBox();
            this.txt_Posc_Codbar = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.txt_rotuloUnido = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txt_Rot_Y = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txt_Rot_x = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CB_LETRA_stilo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.Txt_letra_Tamano = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.barcodeValueTextBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.txt_ciclo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.Lbl_Duracion = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lbl_Hinicio = new System.Windows.Forms.Label();
            this.PBCargue = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.writerPictureBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // txt_RutaImg
            // 
            this.txt_RutaImg.Location = new System.Drawing.Point(80, 3);
            this.txt_RutaImg.Name = "txt_RutaImg";
            this.txt_RutaImg.Size = new System.Drawing.Size(227, 20);
            this.txt_RutaImg.TabIndex = 0;
            this.txt_RutaImg.Text = "Z:\\42. Nueva EPS\\img\\7TI10000_.tif";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ruta Imagen ";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(93, 565);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Iniciar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 565);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Unir CB";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // writerPictureBox
            // 
            this.writerPictureBox.BackColor = System.Drawing.Color.White;
            this.writerPictureBox.Location = new System.Drawing.Point(0, 57);
            this.writerPictureBox.Name = "writerPictureBox";
            this.writerPictureBox.Size = new System.Drawing.Size(861, 502);
            this.writerPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.writerPictureBox.TabIndex = 4;
            this.writerPictureBox.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Ruta Destino";
            // 
            // txt_destino
            // 
            this.txt_destino.Location = new System.Drawing.Point(80, 25);
            this.txt_destino.Name = "txt_destino";
            this.txt_destino.Size = new System.Drawing.Size(227, 20);
            this.txt_destino.TabIndex = 6;
            this.txt_destino.Text = "C:\\zebra\\";
            // 
            // btn_Rotulo
            // 
            this.btn_Rotulo.Location = new System.Drawing.Point(174, 565);
            this.btn_Rotulo.Name = "btn_Rotulo";
            this.btn_Rotulo.Size = new System.Drawing.Size(75, 23);
            this.btn_Rotulo.TabIndex = 2;
            this.btn_Rotulo.Text = "Rotulo";
            this.btn_Rotulo.UseVisualStyleBackColor = true;
            this.btn_Rotulo.Click += new System.EventHandler(this.button3_Click);
            // 
            // txt_Eps
            // 
            this.txt_Eps.Location = new System.Drawing.Point(5, 32);
            this.txt_Eps.Name = "txt_Eps";
            this.txt_Eps.Size = new System.Drawing.Size(122, 20);
            this.txt_Eps.TabIndex = 8;
            this.txt_Eps.Text = "POSITIVA COMPAÑIA DE SEGUROS S.A";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "EPS";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "NIT";
            // 
            // txt_Nit
            // 
            this.txt_Nit.Location = new System.Drawing.Point(5, 110);
            this.txt_Nit.Name = "txt_Nit";
            this.txt_Nit.Size = new System.Drawing.Size(126, 20);
            this.txt_Nit.TabIndex = 10;
            this.txt_Nit.Text = "860011153";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "FACTURA";
            // 
            // txt_Factura
            // 
            this.txt_Factura.Location = new System.Drawing.Point(5, 71);
            this.txt_Factura.Name = "txt_Factura";
            this.txt_Factura.Size = new System.Drawing.Size(126, 20);
            this.txt_Factura.TabIndex = 12;
            this.txt_Factura.Text = "NVC038525";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 133);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Radicacion NEPS";
            // 
            // txt_RadicacionNeps
            // 
            this.txt_RadicacionNeps.Location = new System.Drawing.Point(5, 149);
            this.txt_RadicacionNeps.Name = "txt_RadicacionNeps";
            this.txt_RadicacionNeps.Size = new System.Drawing.Size(126, 20);
            this.txt_RadicacionNeps.TabIndex = 14;
            this.txt_RadicacionNeps.Text = "48102295";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 172);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Fecha Rad";
            // 
            // txt_FechaRad
            // 
            this.txt_FechaRad.Location = new System.Drawing.Point(5, 188);
            this.txt_FechaRad.Name = "txt_FechaRad";
            this.txt_FechaRad.Size = new System.Drawing.Size(126, 20);
            this.txt_FechaRad.TabIndex = 16;
            this.txt_FechaRad.Text = "25/04/2017";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 211);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Valor";
            // 
            // Txt_Valor
            // 
            this.Txt_Valor.Location = new System.Drawing.Point(5, 227);
            this.Txt_Valor.Name = "Txt_Valor";
            this.Txt_Valor.Size = new System.Drawing.Size(126, 20);
            this.Txt_Valor.TabIndex = 18;
            this.Txt_Valor.Text = "1\'500.000";
            // 
            // txt_Posc_Codbar
            // 
            this.txt_Posc_Codbar.Location = new System.Drawing.Point(58, 19);
            this.txt_Posc_Codbar.Name = "txt_Posc_Codbar";
            this.txt_Posc_Codbar.Size = new System.Drawing.Size(39, 20);
            this.txt_Posc_Codbar.TabIndex = 20;
            this.txt_Posc_Codbar.Text = "95";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "posicion";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(748, 565);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(121, 23);
            this.button3.TabIndex = 22;
            this.button3.Text = "Crea rotulo";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(313, 8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 13);
            this.label11.TabIndex = 24;
            this.label11.Text = "Ruta Rotulo";
            // 
            // txt_rotuloUnido
            // 
            this.txt_rotuloUnido.Location = new System.Drawing.Point(383, 4);
            this.txt_rotuloUnido.Name = "txt_rotuloUnido";
            this.txt_rotuloUnido.Size = new System.Drawing.Size(224, 20);
            this.txt_rotuloUnido.TabIndex = 23;
            this.txt_rotuloUnido.Text = "C:\\zebra\\Rotulo1.tif";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(875, 565);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(121, 23);
            this.button4.TabIndex = 25;
            this.button4.Text = "Proceso Completo";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.txt_Rot_Y);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txt_Rot_x);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txt_Posc_Codbar);
            this.groupBox1.Location = new System.Drawing.Point(867, 58);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(137, 100);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Rotulo";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 73);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(20, 13);
            this.label13.TabIndex = 25;
            this.label13.Text = "Y :";
            // 
            // txt_Rot_Y
            // 
            this.txt_Rot_Y.Location = new System.Drawing.Point(58, 70);
            this.txt_Rot_Y.Name = "txt_Rot_Y";
            this.txt_Rot_Y.Size = new System.Drawing.Size(39, 20);
            this.txt_Rot_Y.TabIndex = 24;
            this.txt_Rot_Y.Text = "250";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 48);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(20, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "X :";
            // 
            // txt_Rot_x
            // 
            this.txt_Rot_x.Location = new System.Drawing.Point(58, 45);
            this.txt_Rot_x.Name = "txt_Rot_x";
            this.txt_Rot_x.Size = new System.Drawing.Size(39, 20);
            this.txt_Rot_x.TabIndex = 22;
            this.txt_Rot_x.Text = "400";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CB_LETRA_stilo);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.Txt_letra_Tamano);
            this.groupBox2.Location = new System.Drawing.Point(867, 164);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(137, 100);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Letra";
            // 
            // CB_LETRA_stilo
            // 
            this.CB_LETRA_stilo.FormattingEnabled = true;
            this.CB_LETRA_stilo.Items.AddRange(new object[] {
            "BOLD",
            "ITALIC",
            "REGULAR",
            "STRIKEUOT",
            "UNDERLINE"});
            this.CB_LETRA_stilo.Location = new System.Drawing.Point(57, 45);
            this.CB_LETRA_stilo.Name = "CB_LETRA_stilo";
            this.CB_LETRA_stilo.Size = new System.Drawing.Size(73, 21);
            this.CB_LETRA_stilo.TabIndex = 28;
            this.CB_LETRA_stilo.SelectedValueChanged += new System.EventHandler(this.CB_LETRA_stilo_SelectedValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 73);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(20, 13);
            this.label14.TabIndex = 25;
            this.label14.Text = "Y :";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(58, 70);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(39, 20);
            this.textBox1.TabIndex = 24;
            this.textBox1.Text = "200";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 48);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(31, 13);
            this.label15.TabIndex = 23;
            this.label15.Text = "estilo";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(46, 13);
            this.label16.TabIndex = 21;
            this.label16.Text = "Tamaño";
            // 
            // Txt_letra_Tamano
            // 
            this.Txt_letra_Tamano.Location = new System.Drawing.Point(58, 19);
            this.Txt_letra_Tamano.Name = "Txt_letra_Tamano";
            this.Txt_letra_Tamano.Size = new System.Drawing.Size(39, 20);
            this.Txt_letra_Tamano.TabIndex = 20;
            this.Txt_letra_Tamano.Text = "15";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.txt_Eps);
            this.groupBox3.Controls.Add(this.txt_Factura);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txt_Nit);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.Txt_Valor);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.txt_RadicacionNeps);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.txt_FechaRad);
            this.groupBox3.Location = new System.Drawing.Point(867, 270);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(137, 258);
            this.groupBox3.TabIndex = 29;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Cuerpo Rotulo";
            // 
            // barcodeValueTextBox
            // 
            this.barcodeValueTextBox.Location = new System.Drawing.Point(406, 29);
            this.barcodeValueTextBox.Name = "barcodeValueTextBox";
            this.barcodeValueTextBox.Size = new System.Drawing.Size(126, 20);
            this.barcodeValueTextBox.TabIndex = 20;
            this.barcodeValueTextBox.Text = "01234567";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(313, 30);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(87, 13);
            this.label17.TabIndex = 21;
            this.label17.Text = "Codigo de barras";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(533, 27);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 30;
            this.button5.Text = "Crear";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // txt_ciclo
            // 
            this.txt_ciclo.Location = new System.Drawing.Point(649, 3);
            this.txt_ciclo.Name = "txt_ciclo";
            this.txt_ciclo.Size = new System.Drawing.Size(69, 20);
            this.txt_ciclo.TabIndex = 31;
            this.txt_ciclo.Text = "100";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(613, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Ciclo";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(847, 7);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(60, 13);
            this.label18.TabIndex = 33;
            this.label18.Text = "Hora inicio:";
            // 
            // Lbl_Duracion
            // 
            this.Lbl_Duracion.AutoSize = true;
            this.Lbl_Duracion.Location = new System.Drawing.Point(907, 34);
            this.Lbl_Duracion.Name = "Lbl_Duracion";
            this.Lbl_Duracion.Size = new System.Drawing.Size(49, 13);
            this.Lbl_Duracion.TabIndex = 36;
            this.Lbl_Duracion.Text = "00.00.00";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(847, 34);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 13);
            this.label19.TabIndex = 34;
            this.label19.Text = "Duracion:";
            // 
            // lbl_Hinicio
            // 
            this.lbl_Hinicio.AutoSize = true;
            this.lbl_Hinicio.Location = new System.Drawing.Point(907, 7);
            this.lbl_Hinicio.Name = "lbl_Hinicio";
            this.lbl_Hinicio.Size = new System.Drawing.Size(49, 13);
            this.lbl_Hinicio.TabIndex = 35;
            this.lbl_Hinicio.Text = "00.00.00";
            // 
            // PBCargue
            // 
            this.PBCargue.Location = new System.Drawing.Point(255, 565);
            this.PBCargue.Name = "PBCargue";
            this.PBCargue.Size = new System.Drawing.Size(487, 23);
            this.PBCargue.TabIndex = 37;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1007, 597);
            this.Controls.Add(this.PBCargue);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.Lbl_Duracion);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.lbl_Hinicio);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_ciclo);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.barcodeValueTextBox);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txt_rotuloUnido);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_destino);
            this.Controls.Add(this.writerPictureBox);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btn_Rotulo);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_RutaImg);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.writerPictureBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_RutaImg;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox writerPictureBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_destino;
        private System.Windows.Forms.Button btn_Rotulo;
        private System.Windows.Forms.TextBox txt_Eps;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_Nit;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_Factura;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_RadicacionNeps;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_FechaRad;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox Txt_Valor;
        private System.Windows.Forms.TextBox txt_Posc_Codbar;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txt_rotuloUnido;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txt_Rot_Y;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txt_Rot_x;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox Txt_letra_Tamano;
        private System.Windows.Forms.ComboBox CB_LETRA_stilo;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox barcodeValueTextBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox txt_ciclo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label Lbl_Duracion;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lbl_Hinicio;
        private System.Windows.Forms.ProgressBar PBCargue;
    }
}

