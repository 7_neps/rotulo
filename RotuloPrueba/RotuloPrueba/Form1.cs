﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using BarcodeDemo;

using Vintasoft.Barcode;
using Vintasoft.Barcode.BarcodeInfo;
using Vintasoft.Barcode.SymbologySubsets;
using Vintasoft.Barcode.SymbologySubsets.GS1;
using Vintasoft.Barcode.SymbologySubsets.RoyalMailMailmark;
using Vintasoft.Barcode.GS1;
using System.Collections.Generic;
using iTextSharp.text.pdf;

namespace RotuloPrueba
{
    public partial class Form1 : Form
    {
        int i = 0;
        string Formato = ".tif";
        
        public Form1()
        {
            InitializeComponent();

        }

        public Bitmap SuperimposeRotulo(Bitmap largeBmp, Bitmap smallBmp)
        {
            Graphics g = Graphics.FromImage(largeBmp);
            g.CompositingMode = CompositingMode.SourceOver;
            smallBmp.MakeTransparent();
       
            int x = 55;
            int y = int.Parse(txt_Posc_Codbar.Text);
            //
            g.DrawImage(smallBmp, new Point(x, y));
            return largeBmp;
        }

        public Bitmap Superimpose(Bitmap largeBmp, Bitmap smallBmp)
        {
            Graphics g = Graphics.FromImage(largeBmp);
            g.CompositingMode = CompositingMode.SourceCopy;
            smallBmp.MakeTransparent();
            int margin = 5;
            int x = largeBmp.Width - smallBmp.Width - margin;
            //int y = largeBmp.Height - smallBmp.Height - margin;
            //int x = 0;
            int y = 0;
            //
            g.DrawImage(smallBmp, new Point(x, y));
            return largeBmp;
        }
       
        private void button1_Click(object sender, EventArgs e)
        {
            int x = 0;
            int y = 0;
            int AL = 200;
            int AN = 200;
            int umbal = 10;
            int separacion = umbal + 200;
            byte[] imgData = getData(txt_RutaImg.Text);

            // En IMG esta la imagen Padre
            System.Drawing.Image img = System.Drawing.Image.FromStream(new MemoryStream(imgData));
            // esta el codigo de barras
            Bitmap pez = new Bitmap(txt_destino.Text + "\\Rotulo" + i.ToString() + Formato);
            Bitmap bm = new Bitmap(new Bitmap(img));
            //Resultado de la union
            Bitmap Resultado = Superimpose(bm, pez);
            // Visualizar la union
            writerPictureBox.Image = Resultado;
            Resultado.Save(txt_destino.Text + "\\Prueba" + i.ToString() + Formato, System.Drawing.Imaging.ImageFormat.Tiff);

            Resultado.Dispose();
            pez.Dispose();
            bm.Dispose();
            File.Delete(txt_destino.Text + "\\Rotulo" + i.ToString() + Formato);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Bitmap bm = new Bitmap(txt_destino.Text + "\\Rotulo0" + Formato);
            Bitmap pez = new Bitmap(txt_destino.Text + "\\Codebar__" + Formato);
            Bitmap Resultado = SuperimposeRotulo(bm, pez);
            Resultado.Save(txt_destino.Text + "\\RotuloUnido" + Formato, System.Drawing.Imaging.ImageFormat.Tiff);
            writerPictureBox.Image = Resultado;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            writerPictureBox.Image = null;
            float x = 10.0F;
            float y = 10.0F;
            float x1 = 60;
            float y1 = int.Parse(txt_Posc_Codbar.Text);
            Font drawFont;

            switch (CB_LETRA_stilo.SelectedIndex.ToString())
            {
                case "0":
                      drawFont = new Font("Arial", int.Parse(Txt_letra_Tamano.Text), FontStyle.Bold, GraphicsUnit.Pixel);
                    break;
                case "1":
                      drawFont = new Font("Arial", int.Parse(Txt_letra_Tamano.Text), FontStyle.Italic, GraphicsUnit.Pixel);
                    break;
                case "2":
                    drawFont = new Font("Arial", int.Parse(Txt_letra_Tamano.Text), FontStyle.Regular, GraphicsUnit.Pixel);
                    break;
                case "3":
                    drawFont = new Font("Arial", int.Parse(Txt_letra_Tamano.Text), FontStyle.Strikeout, GraphicsUnit.Pixel);
                    break;
                case "4":
                    drawFont = new Font("Arial", int.Parse(Txt_letra_Tamano.Text), FontStyle.Underline, GraphicsUnit.Pixel);
                    break;
                default:
                      drawFont = new Font("Arial", int.Parse(Txt_letra_Tamano.Text), FontStyle.Bold, GraphicsUnit.Pixel);
                    break;
            }

 

            
            SolidBrush drawBrush = new SolidBrush(System.Drawing.Color.Black);
            StringFormat drawFormat = new StringFormat();
            var image = new Bitmap(int.Parse(txt_Rot_x.Text), int.Parse(txt_Rot_Y.Text));
            string Nit = " NIT: " + txt_Nit.Text;
            string Eps = txt_Eps.Text;
            string Factura = txt_Factura.Text;
            string FechaR = txt_FechaRad.Text;
            string valor = "$ " + Txt_Valor.Text;
            string EpsLinea1 = "";
            string EpsLinea2 = "";

            string[] dd = Eps.Split(' ');

            foreach (string datos in dd)
            {
                string temporal = EpsLinea1 + datos;
                if (temporal.Length <= 22)
                {
                    EpsLinea1 = EpsLinea1 + datos + " ";
                }
                else
                {
                    EpsLinea2 = EpsLinea2 + datos + " ";
                    if (EpsLinea2.Length >= 25) {EpsLinea2 = EpsLinea2.Substring(0, 24); }
                }
            }

            int CantidadCaracteres = EpsLinea1.Replace(" ","").Length;
            int TotalEspacio = EpsLinea1.Length - CantidadCaracteres;
            int diferencia = CantidadCaracteres + (TotalEspacio*2);



            string  drawString =    "\n";
            drawString = drawString + "  " + EpsLinea1.PadRight(22, ' ') + Nit.PadLeft((45 - diferencia), ' ') + "\n";

            //drawString = drawString + ("  " + EpsLinea1.PadRight(22, ' ') + Nit.PadLeft((55 - 22), ' ')).Length.ToString()+ "\n";

            if (EpsLinea2 != "") { drawString = drawString + "  " + EpsLinea2 + "\n"; } else { drawString = drawString + "\n";  }
            drawString = drawString + "FACTURA".PadLeft(50,' ') +" "+ Factura + "\n";
            drawString = drawString + "Radicacion NEPS:    ".PadRight(29, ' ') + txt_RadicacionNeps.Text + "\n";
            drawString = drawString + "\n\n\n";
            drawString = drawString + "  Fecha de rad:      ".PadRight(35,' ') + FechaR  + "\n\n";
            drawString = drawString + "  Valor: " + valor + "\n";

            Graphics formGraphics = Graphics.FromImage(image);

        //   Font drawFont2 = new Font("IDAutomationSC128XXS DEMO", 30, FontStyle.Regular, GraphicsUnit.Pixel);
            Font drawFont2 = new Font("Free 3 of 9 Extended", 70, FontStyle.Regular, GraphicsUnit.Pixel);


            formGraphics.DrawString(drawString, drawFont, drawBrush, x, y, drawFormat);
            formGraphics.DrawString(txt_RadicacionNeps.Text, drawFont2, drawBrush, x1, y1, drawFormat);
            
            writerPictureBox.Image = image;
            image.Save(txt_destino.Text + "\\Rotulo" + i.ToString() + Formato, System.Drawing.Imaging.ImageFormat.Tiff);


            image.Dispose();
            drawFont.Dispose();
            drawBrush.Dispose();
            formGraphics.Dispose();

        }

        private Image Dibujar(Bitmap bm, int x, int y, int AL, int AN, Color color)
        {

            using (Graphics gr = Graphics.FromImage(bm))
            {
                //int left = 0;
                //int top = 0;
                //int right = 200;
                //int bottom = 200;

                Point[] puntos = { new Point(x, y), new Point(AL, y), new Point(AL, AN), new Point(x, AN) };
                gr.FillPolygon(new SolidBrush(Color.FromArgb(100, color)), puntos);
                gr.DrawPolygon(new Pen(color), puntos);
            }
            return bm;


        }

        public byte[] getData(string filePath)
        {


            FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);


            BinaryReader br = new BinaryReader(fs);


            byte[] data = br.ReadBytes((int)fs.Length);


            br.Close();


            fs.Close();


            return data;


        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            button3_Click(null,null);
            button2_Click(null, null);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            PBCargue.Minimum = 0;
            PBCargue.Maximum = int.Parse(txt_ciclo.Text);
            PBCargue.Value = 0;

            DateTime hora_inicio = DateTime.Now;
            lbl_Hinicio.Text = hora_inicio.ToShortTimeString();
            for (i = 0; i < int.Parse(txt_ciclo.Text); i++)
            {
                DateTime newDate = DateTime.Now;
                TimeSpan ts = newDate - hora_inicio;
                Lbl_Duracion.Text = ts.ToString();
                Application.DoEvents();
                button3_Click(null, null);
                button1_Click(null, null);
                AumentaPB();
            }
            MessageBox.Show("Fin proceso");

        }

        public void AumentaPB()
        {
            try
            {
                //aumenta el valor del PB
                PBCargue.Value += 1;
                Application.DoEvents();
            }
            catch (Exception ex)
            {
            }

        }
       
        private void CB_LETRA_stilo_SelectedValueChanged(object sender, EventArgs e)
        {
            button3_Click(null,null);
        }

        private void SaveMultiPage(string sOutFilePath)
        {
            //variables
           // string sOutFilePath = @"Z:\42. Nueva EPS\img\pagina1\page1.tiff";


            Bitmap bitmap = (Bitmap)Image.FromFile(@"Z:\42. Nueva EPS\img\BK\7TI10000_.png");
            //Save the bitmap to memory as tiff

            MemoryStream byteStream = new MemoryStream();
            bitmap.Save(byteStream, ImageFormat.Tiff);
            //Put Tiff into another Image object

            Image tiff = Image.FromStream(byteStream);
            //Prepare encoders:

            ImageCodecInfo encoderInfo = GetEncoderInfo("image/tiff");

            EncoderParameters encoderParams = new EncoderParameters(2);
            EncoderParameter parameter = new EncoderParameter(System.Drawing.Imaging.Encoder.Compression, (long)EncoderValue.CompressionCCITT4);
            encoderParams.Param[0] = parameter;
            parameter = new EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag, (long)EncoderValue.MultiFrame);
            encoderParams.Param[1] = parameter;
            //Save to file:

            tiff.Save(sOutFilePath, encoderInfo, encoderParams);
            //For subsequent pages, prepare encoders:



            Image sOutFilePath2 = Image.FromFile(@"Z:\42. Nueva EPS\img\BK\Codebar__.png");
            EncoderParameters EncoderParams = new EncoderParameters(2);
            EncoderParameter SaveEncodeParam = new EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag, (long)EncoderValue.FrameDimensionPage);
            EncoderParameter CompressionEncodeParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Compression, (long)EncoderValue.CompressionCCITT4);
            EncoderParams.Param[0] = CompressionEncodeParam;
            EncoderParams.Param[1] = SaveEncodeParam;
            tiff.SaveAdd(sOutFilePath2, EncoderParams);
            //Finally flush the file:

            EncoderParameter SaveEncodeParam2 = new EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag, (long)EncoderValue.Flush);
            EncoderParams = new EncoderParameters(1);
            EncoderParams.Param[0] = SaveEncodeParam2;

            tiff.SaveAdd(EncoderParams);
        }

        private void SaveMultiPage2(string fileName)
        {

            string[] _scannedPages = new string[3];
            List<Image> Resultado = GetAllPages(@"C:\Ecatch\repositorio\NEPs\system\2018\152\67\Temp\1.tif");
          

            System.Drawing.Imaging.Encoder encoder = System.Drawing.Imaging.Encoder.SaveFlag;
            System.Drawing.Imaging.Encoder compressionEncoder;
            ImageCodecInfo encoderInfo = GetEncoderInfo("image/tiff");
            EncoderParameters encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = new EncoderParameter(encoder, (long)EncoderValue.MultiFrame);

            // Save the first frame of the multi page tiff
            //Bitmap bitmapFirst = (Bitmap)Image.FromFile(@"Z:\42. Nueva EPS\img\7TI10000_.tif");
            compressionEncoder = System.Drawing.Imaging.Encoder.Compression;
            Bitmap firstImage = (Bitmap)Image.FromFile(@"C:\Ecatch\repositorio\NEPs\Rotulo\111.tif");
            firstImage.Save(@"C:\Ecatch\repositorio\NEPs\system\2018\152\67\1.tif", encoderInfo, encoderParameters);

            encoderParameters.Param[0] = new EncoderParameter(encoder, (long)EncoderValue.FrameDimensionPage);

            // Add the remining images to the tiff
            for (int i = 0; i < Resultado.Count; i++)
            {
                Bitmap img = (Bitmap)Resultado[i];
                firstImage.SaveAdd(img, encoderParameters);
            }

            // Close out the file
            encoderParameters.Param[0] = new EncoderParameter(encoder, (long)EncoderValue.Flush);
            firstImage.SaveAdd(encoderParameters);

        }

        private void SaveMultiPage3(string imagePath)
        {
            PdfReader pdf = new PdfReader(@"Z:\42. Nueva EPS\img\BK\88.tif");
            PdfStamper stp = new PdfStamper(pdf, new FileStream(@"Z:\42. Nueva EPS\img\pagina1\Resultado.pdf", FileMode.Create));
            PdfWriter writer = stp.Writer;
            iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(@"Z:\42. Nueva EPS\img\Prueba0.tif");
            PdfDictionary pg = pdf.GetPageN(1);
            PdfDictionary res =
            (PdfDictionary)PdfReader.GetPdfObject(pg.Get(PdfName.RESOURCES));
            PdfDictionary xobj =
            (PdfDictionary)PdfReader.GetPdfObject(res.Get(PdfName.XOBJECT));
            if (xobj != null)
            {
                foreach (PdfName name in xobj.Keys)
                {
                    PdfObject obj = xobj.Get(name);
                    if (obj.IsIndirect())
                    {
                        PdfDictionary tg = (PdfDictionary)PdfReader.GetPdfObject(obj);
                        PdfName type =
                        (PdfName)PdfReader.GetPdfObject(tg.Get(PdfName.SUBTYPE));
                        if (PdfName.IMAGE.Equals(type))
                        {
                            PdfReader.KillIndirect(obj);
                            iTextSharp.text.Image maskImage = img.ImageMask;
                            if (maskImage != null)
                                writer.AddDirectImageSimple(maskImage);
                            writer.AddDirectImageSimple(img, (PRIndirectReference)obj);
                            break;
                        }
                    }
                }
            }
            stp.Close();
        }




        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

        private ImageCodecInfo GetEncoder(ImageFormat format)
        {

            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            SaveMultiPage2(@"Z:\42. Nueva EPS\img\pagina1\Prueba1.tif");

        }

        private List<Image> GetAllPages(string file)
        {
            List<Image> images = new List<Image>();
            Bitmap bitmap = (Bitmap)Image.FromFile(file);
            int count = bitmap.GetFrameCount(FrameDimension.Page);

            for (int idx = 0; idx < count; idx++)
            {
                // save each frame to a bytestream
                MemoryStream byteStream = new MemoryStream();
                if (idx == 0)
                {
                    //Bitmap bitmapFirst = (Bitmap)Image.FromFile(@"Z:\42. Nueva EPS\img\7TI10000_.tif");
                    //bitmapFirst.SelectActiveFrame(FrameDimension.Page, 0);
                    //bitmapFirst.Save(byteStream, ImageFormat.Tiff);
                    //images.Add(Image.FromStream(byteStream));
                   // Save the bitmap to memory as tiff
                    bitmap.SelectActiveFrame(FrameDimension.Page, idx);
                    bitmap.Save(byteStream, ImageFormat.Tiff);
                    images.Add(Image.FromStream(byteStream));
                }
                else
                {
                    bitmap.SelectActiveFrame(FrameDimension.Page, idx);
                    bitmap.Save(byteStream, ImageFormat.Tiff);
                    images.Add(Image.FromStream(byteStream));
                }


                // and then create a new Image from it
                //images.Add(Image.FromStream(byteStream));
            }
            return images;
        }
    }
}
